<?php
    $filepath = realpath(dirname(__FILE__));
    include_once ($filepath.'/inc/header.php');
    include_once($filepath .'/../classes/Exam.php');
    $exm =new Exam();

?>
    <style>
        .adminpanel{
            width:600px;
            color:#999;
            margin: 20px auto 0;
            padding: 10px;
            border: 1px solid #dddddd;
        }
    </style>
    <?php
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $addQue = $exm->addquestions($_POST);
        }
        //Get total
        $total = $exm->getTotalRows();
        $next  = $total + 1;


    ?>

    <div class="main">
        <h1>Add Question </h1>
        <div class="adminpanel">
           <form action="" method="post">
               <table>
                   <tr>
                       <td>Question no.</td>
                       <td>:</td>
                       <td><input type="number" value="<?php
                            if(isset($next)){
                                echo $next;}?> " name="quesNo" placeholder="Enter question No...." required /></td>


                   </tr>
                   <tr>
                       <td>Question</td>
                       <td>:</td>
                       <td><input type="text"  name="ques" placeholder="Enter question...." required /></td>

                   </tr>
                   <tr>
                       <td>Choice one:</td>
                       <td>:</td>
                       <td><input type="text"  name="ans1" placeholder="Enter your Choice one" required /></td>

                   </tr>
                   <tr>
                       <td>Choice two:</td>
                       <td>:</td>
                       <td><input type="text"  name="ans2" placeholder="Enter your Choice two" required /></td>

                   </tr>
                   <tr>
                       <td>Choice three:</td>
                       <td>:</td>
                       <td><input type="text"  name="ans3" placeholder="Enter your Choice three" required /></td>

                   </tr>
                   <tr>
                       <td>Choice four:</td>
                       <td>:</td>
                       <td><input type="text"  name="ans4" placeholder="Enter your Choice four" required /></td>

                   </tr>
                   <tr>
                       <td>Correct No:</td>
                       <td>:</td>
                       <td><input type="number"  name="right" placeholder="Enter Right answer" required /></td>

                   </tr>
                   <tr>
                       <td colspan="3" align="center">
                           <input  type="submit" value="Add a question" >
                       </td>

                   </tr>

               </table>

           </form>
        </div>



    </div>
<?php include 'inc/footer.php'; ?>