<?php
    $filepath = realpath(dirname(__FILE__));
    include_once ($filepath.'/inc/header.php');
    include_once($filepath.'/../classes/Users.php');

    $usr =new Users();
?>
<?php

    if(isset($GET['dis'])){
        $dblid =(int)$_GET['dis'];
        $dblUser =$usr->DisableUser($dblid);
    }
?>



    <div class="main">
        <h1>Admin Panel -Manage Users</h1>
        <?php
            if(isset($dblUser)){
                echo $dblUser;
                    }
        ?>
        <div class="manageuser">
            <table class="tblone">
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>UserName</th>
                    <th>Email</th>
                    <th>Action</th>

                    <?php

                        $userData = $usr->getAllUsers();
                        if($userData){
                            $i=0;
                            while($result = $userData->fetch_assoc()){
                              $i++;
                    ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $result['name'];?></td>
                    <td><?php echo $result['username'];?></td>
                    <td><?php echo $result['email'];?></td>
                    <td>
                        <?php if($result['status']=='0') { ?>
                            <a onclick="return confirm('are u sure to Disable')"
                            href="?dis= <?php echo $result['UserId'];?>" ">Disable</a>
                           <?php }else {?>
                            <a onclick="return confirm('are u sure to Enable')"
                                   href="?ena= <?php echo $result['UserId'];?>">Enable</a>
                            <?php }?>
                                      || <a onclick="return confirm('are u sure to Remove')"
                                     href="?del= <?php echo $result['UserId'];?>">Remove</a>

                    </td>
                </tr>
                    <?php }  }?>
            </table>
        </div>
    </div>
<?php include 'inc/footer.php'; ?>