


    $(function(){
        //For User Registration
        $("#regsSubmit").click(function(){
            var name        =$("#name").val();
            var username    =$("#username").val();
            var password    =$("#pass").val();
            var email       =$("#email").val();
            var dataString  ='name='+name+'&username='+username+'&password='
                +password+'&email='+email;
            $.ajax({
                type:"POST",
                url:"getregister.php",
                data:dataString,
                success:function(data){
                    $("#state").html(data);
                }
            });
            return false;
        });
        //For userLogin
        $("#loginsubmit").click(function(){
           var email        =$("#email").val();
           var password     =$("#password").val();
           var dataString   ='email='+email+'&password='+password;
            $.ajax({
                type:"POST",
                url:"getLogin.php",
                data:dataString,
                success:function(data){
                    if($.trim(data)=="empty"){
                        $(".empty").show();
                        $(".disable").hide();
                        $(".error").hide();

                    }else if($.trim(data)=="disable"){
                        $(".empty").hide();
                        $(".disable").show();
                        $(".error").hide();

                    }else if($.trim(data)=="error"){
                        $(".empty").hide();
                        $(".disable").hide();
                        $(".error").show();

                    }else{
                        window.location ="exam.php";
                    }
                }


            })
            return false;
        });
    });