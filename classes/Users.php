<?php

    $filepath =realpath(dirname(__FILE__));
    include_once($filepath . '../../lib/Session.php');
    include_once($filepath . '../../lib/Database.php');
    include_once($filepath . '../../helpers/Format.php');


/**
 * Class users
 */

class Users
{
    private  $db;
    private  $fm;

    public function __construct()
    {
        $this->db =new Database();
        $this->fm =new Format();

    }
    public function userRegistration($name, $username,$password, $email){
        $name            =    $this->fm->validation('$name');
        $username        =    $this->fm->validation('$username');
        $password        =    $this->fm->validation('$password');
        $email           =    $this->fm->validation('$email');

        $name       =    mysqli_real_escape_string($this->db->link,$name);
        $username   =    mysqli_real_escape_string($this->db->link,$username);
        $password   =    mysqli_real_escape_string($this->db->link,md5($password));
        $email      =    mysqli_real_escape_string($this->db->link,$email);

        if($name =="" || $username =="" || $password =="" || $email ==""){
            echo "<span class='error'>Fields Must Not Be Empty</span>";
            exit();
        }else if(filter_var($email,FILTER_VALIDATE_EMAIL)==false){
            echo "<span class='error'>Invalid Email Address</span>";
            exit();
        }else{
            $chkquery ="SELECT * FROM 	user_tbl WHERE email='$email' ";
            $chkresult =$this->db->select($chkquery);
            if($chkresult !=false){
                echo "<span class='error'> Email already Exit</span>";
                exit();
            }else{
                $query ="INSERT INTO user_tbl(name,username,password,email) VALUES ('$name,$username,$password,$email')";
                $inserted_row=$this->db->insert($query);
                if($inserted_row){
                    echo "<span class='success'>Registration Successfully</span>";
                    exit();
                }else{
                    echo "<span class='error'> Not Registration</span>";
                    exit();
                }
            }
        }

    }

    public function userLogin($email,$password){
        $email           =    $this->fm->validation('$email');
        $password        =    $this->fm->validation('$password');

        $email      =    mysqli_real_escape_string($this->db->link,$email);
        $password   =    mysqli_real_escape_string($this->db->link,$password);

        if($email =="" || $password =="" ){
            echo "empty";
            exit();
        }else {
            $query = "SELECT *FROM user_tbl WHER email='$email' AND password='$password' ";
            $result =$this->db->select($query);

            if($result !=false){
                $value =$result->fetch_assoc();

                if($value['status']== '1'){
                    echo "disable";
                    exit();
                }else{

                }
            }
        }
    }

    public function getAllUsers(){
        $query = "SELECT *FROM user_tbl ORDER BY UserId DESC ";
        $result =$this->db->select($query);
        return $result;

    }
    public function DisableUser($UserId){
        $query ="UPDATE user_tbl SET status ='1' WHERE UserId='UserId' ";
        $updated_row =$this->db->update($query);
        if($updated_row){
            $msg ="<span class='success'>User Disable !</span>";
            return $msg;
        }else{
            $msg ="<span class='error'>User Not Disable !</span>";
            return $msg;
        }
    }
}
?>